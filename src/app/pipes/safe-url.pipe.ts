import { ElementRef, Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'safeUrl'
})
export class SafeUrlPipe implements PipeTransform {

  constructor(
    private domSanitizer: DomSanitizer,
    private elementRef: ElementRef
  ) {
  }

  transform(url: string | null) {
    url = url ?? ' '
    return this.domSanitizer.bypassSecurityTrustResourceUrl(url)
  }

}

