import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    protected httpClient: HttpClient
  ) {
  }

  public fetchQrCode(text: string) {
    return this.httpClient.get(`https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=${text}`,
      {responseType: 'blob'}
    ).pipe(
      map((data: Blob) => {
          return window.URL.createObjectURL(data);
        }
      )
    );
  }
}
