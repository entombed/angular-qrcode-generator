export class QrCodeGet {
  static readonly type = '[QRCODE] Get';
  constructor( public text: string ) {
  }
}
