import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { ApiService } from '../services/api.service';
import { map } from 'rxjs';
import { IQrCodeGet } from '../interfaces/i-qr-code-get';
import { QrCodeGet } from './actions';



interface IQrCodeState {
  qrUrl: string;
  isLoading: boolean,
  isActive: boolean
}

@State<IQrCodeState>({
  name: 'qrCode',
  defaults: {
    qrUrl: '',
    isLoading: false,
    isActive: false,
  }
})

@Injectable()
export class QrCodeState {

  constructor(
    protected apiService: ApiService
  ) {
  }

  @Selector()
  static qrCodeUrl(state: IQrCodeState) {
    return state.qrUrl;
  }

  @Selector()
  static isLoading(state: IQrCodeState) {
    return state.isLoading;
  }

  @Selector()
  static isActive(state: IQrCodeState) {
    return state.isActive;
  }

  @Action(QrCodeGet)
  fetchQrCode(
    ctx: StateContext<IQrCodeState>,
    payload: IQrCodeGet
  ) {
    const {
      text
    } = payload
    ctx.patchState({
      isLoading: true,
      isActive: false
    })
    this.apiService.fetchQrCode(text).pipe(
      map((data: string) => {
        ctx.patchState({
          qrUrl: data,
          isLoading: false,
          isActive: true
        })
      })
    ).subscribe()
  }

}
