import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { QrCodeState } from '../../ngxs-store/state';
import { Observable } from 'rxjs';
import { QrCodeGet } from '../../ngxs-store/actions';

@Component({
  selector: 'app-qr-code-form',
  templateUrl: './qr-code-form.component.html',
  styleUrls: ['./qr-code-form.component.scss']
})
export class QrCodeFormComponent implements OnInit {

  @Select(QrCodeState.qrCodeUrl) qrCodeUrl$!: Observable<string>
  @Select(QrCodeState.isActive) isActive$!: Observable<boolean>
  @Select(QrCodeState.isLoading) isLoading$!: Observable<boolean>

  public buttonLabel: string = 'Generate QR Code'
  constructor(
    protected store: Store
  ) { }

  ngOnInit(): void {
  }

  public onClickGenerateCode(text: string) {
    this.store.dispatch( new QrCodeGet(text))
  }

}
