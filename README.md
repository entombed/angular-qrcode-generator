# Simple QR Code generator

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.8.

Simple QR Code generation from inputting string

For generation code use [API goqr.me](https://goqr.me/api)

Live demo [angular-qrcode-generator-2022.web.app](https://angular-qrcode-generator-2022.web.app/) 
